Source: mew-beta
Section: mail
Priority: optional
Maintainer: Tatsuya Kinoshita <tats@debian.org>
Build-Depends: debhelper-compat (= 13), zlib1g-dev (>= 1:1.1.3)
Build-Depends-Indep: texinfo
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/debian/mew-beta
Vcs-Git: https://salsa.debian.org/debian/mew-beta.git
Homepage: http://www.mew.org/
Rules-Requires-Root: binary-targets

Package: mew-beta
Section: lisp
Architecture: all
Depends: mew-beta-bin (>= 5.2.53), emacsen-common (>= 2.0.8), emacs-nox | emacs | emacs-snapshot, dpkg (>= 1.15.4) | install-info, ${misc:Depends}
Suggests: sensible-utils, w3m-el, gnupg | gnupg2 | gnupg1, gpgsm, ssh, wv, xlhtml, ppthtml, compface, netpbm, x-face-el, mu-cite, mule-ucs, bogofilter | bsfilter | spamassassin, hyperestraier, namazu2, namazu2-index-tools, mhc
Replaces: mew
Conflicts: mew
Provides: mew, mail-reader, imap-client, news-reader
Description: mail reader supporting PGP/MIME for Emacs (development version)
 Mew (Messaging in the Emacs World) is a user interface for text messages,
 multimedia messages (MIME), news articles and security functionality
 including PGP, S/MIME, SSH and SSL.
 .
 The features of Mew are as follows:
 .
  - POP, SMTP, NNTP and IMAP are supported.
  - You can easily display a very complicated structured message.
  - You can start to read messages before they are all fully listed.
  - For refiling, default folders are neatly suggested.
  - You can complete field names, e-mail addresses, receiver's names,
    domain names and folder names.
  - You can easily search messages with keywords etc.
  - Thread, a mechanism to display the flow of messages, is supported.
 .
 This package provides a development snapshot version of Mew.  To use a
 stable version of Mew, install the mew package instead of this package.

Package: mew-beta-bin
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: stunnel4 | stunnel, ca-certificates, ruby, ruby-sqlite3
Suggests: hyperestraier
Enhances: mew-beta, gnupg-agent
Replaces: mew-bin
Conflicts: mew-bin, mew-beta (<< 4)
Provides: mew-bin, mail-reader, pinentry
Description: external commands for Mew (development version)
 The mew-beta-bin package contains external commands for the mew-beta package.
 .
  - mewencode: encode/decode MIME objects
  - mewl: extract necessary fields from messages stored in folders
  - incm: incorporate new mails from maildir or mbox to Mew's inbox folder
  - mewest: update indexes of Hyper Estraier
  - mew-pinentry: front end of gpg-agent to ask a passphrase to a user
  - mewstunnel: wrapper script for using stunnel
  - cmew: create Mew's database file
  - smew: search related messages from Mew's database file
